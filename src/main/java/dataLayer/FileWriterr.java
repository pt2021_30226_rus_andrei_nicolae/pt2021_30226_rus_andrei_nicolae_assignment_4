package dataLayer;
import java.io.FileWriter;

import businessLayer.MenuItem;
import businessLayer.Order;
import java.io.IOException;
import java.util.ArrayList;

/**
 * clasa scriere fisier
 */
public class FileWriterr {
    /**
     *
     * @param order
     * @param menuItems
     * @throws IOException
     */
    public static void fileWrite(Order order, ArrayList<MenuItem> menuItems) throws IOException {
        FileWriter myWriter = new FileWriter("Order_"+order.getId()+"_"+order.getClient().getUsername()+".txt"); //creare fisier
        myWriter.write("Order: "+order.getId()); // scriere pe rand etc
        myWriter.write("\nDate: "+order.getDate().getTime()); //la fel
        myWriter.write("\nUsername: "+order.getClient().getUsername());
        myWriter.write("\nProducts ordered: ");
        for(MenuItem menuItem : menuItems) // se ia lista si se scrie fiecare produs
        {
            myWriter.write("\n"+menuItem.toStringg());
        }
        myWriter.write("\nTotal price: "+order.getPrice()); // si pretul totoal
        myWriter.close();
    }
}