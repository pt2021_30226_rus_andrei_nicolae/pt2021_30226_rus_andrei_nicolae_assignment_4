package dataLayer;
import businessLayer.DeliveryService;

import java.io.*;

/**
 *
 */
public class Serializator {
    /**
     *
     * @param deliveryService
     */
    public static void serializator(DeliveryService deliveryService) // metoda de salvarea datelor
    {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("serializator.ser"); // nume fisier
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(deliveryService); // se va scrie obiectul
            out.close();
            fileOut.close();

        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    /**
     *
     * @return
     */
    public static DeliveryService deserializator() // extragerea obiectului
    {
        DeliveryService e;
        try {
            FileInputStream fileIn = new FileInputStream("serializator.ser"); // cautarea obiectului
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (DeliveryService) in.readObject(); // extragerea lui
            in.close();
            fileIn.close();
            return e; // returnarea obiectului
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("DeliveryClass class not found");
            c.printStackTrace();
        }
        return null;
    }
}
