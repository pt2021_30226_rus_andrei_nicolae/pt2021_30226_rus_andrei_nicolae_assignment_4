package businessLayer;

import java.io.Serializable;

/**
 *
 */
public class Client implements Serializable {
    private String username;
    private String password;

    /**
     * @param username
     * @param password
     */
    public Client(String username,String password)
    {
        this.password=password;
        this.username=username;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }
    @Override
    /**
     *
     */
    public String toString()
    {
        return " username: " + username;
    }

}
