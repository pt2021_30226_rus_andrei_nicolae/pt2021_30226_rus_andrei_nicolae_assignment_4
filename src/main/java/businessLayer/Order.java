package businessLayer;
import java.io.Serializable;
import java.util.Calendar;
// clasa comenzi

/**
 * clasa Comanda
 */
public class Order implements Serializable {
    private int id; //  campuri
    private Calendar date;
    private int price;
    public Client client;

    /**
     *
     * @param id id
     * @param date
     * @param price
     * @param client
     */
    public Order(int id,Calendar date,int price,Client client)
    {
        this.id=id;
        this.date=date;
        this.price=price;
        this.client=client;
    }

    /**
     *
     * @return
     */
    public Calendar getDate() {
        return date;
    }

    /**
     *
     * @return
     */
    public int getPrice() {
        return price;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Client getClient() {
        return client;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString()
    {
        return "Order: "+id+"; Date: "+date.DATE+date.getTime()+"; Price: "+price+"; Client: "+client.getUsername();
    }
}
