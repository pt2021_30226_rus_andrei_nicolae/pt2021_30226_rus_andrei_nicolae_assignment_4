package businessLayer;

import dataLayer.Serializator;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.*;
import java.util.stream.*;

/**
 * @invariant isWellFormed()
 */
public class DeliveryService extends Observable implements IDeliveryServiceProcessing, Serializable {

    private HashSet<MenuItem> items; // produse
    private HashMap<Order, ArrayList<MenuItem>> orders; // comenzi
    private ArrayList<Client> registeredClients; // clienti inregistrati
    private Set<Order> task1; // taskuri pentru admin
    private Set<MenuItem> task2;
    private Set<Client> task3;
    private HashSet<MenuItem> task4 = new HashSet<>();
    private ArrayList<Integer>  task4L = new ArrayList<>();
    private Order lastOrder; // ultima comanda
    private final Client administrator; // admin
    private final Client employee; // angajator

    boolean isWellFormed (){
        return true;
    }

    public DeliveryService()
    {
        assert isWellFormed();
        this.administrator = new Client("admin","123"); // admin
        this.employee = new Client("emp","123"); // employee
        this.items = new HashSet<>();
        this.orders=new HashMap<Order,ArrayList<MenuItem>>();
        this.registeredClients=new ArrayList<>();
    }

    /**
     * @param baseProduct produsul de baza de adaugat
     */
    public void addProduct(BaseProduct baseProduct) // adaugare produs
    {
        assert true;
        items.add(baseProduct);
    }

    /**
     * @param name Nume de cautat
     * @param menuItem Nume de editat
     */
    public void editProduct(String name,MenuItem menuItem) // editare
    {
        assert name!=null;
        for (MenuItem item : items) {
            if (item.getTitlee(). equals(name)) { // daca e egal
                item.setFields(menuItem);
                break;
            }
        }
    }

    /**
     * @param name Name to  be deleted
     */
    public void deleteProduct(String name) // stergere produs
    {
        assert name!=null;
        for (MenuItem menuItem : items)
        {
            if(menuItem.getTitlee().equals(name))
            {
                items.remove(menuItem); // stergere
                break;
            }
        }
    }
    /**
     *
     */
    public void importProducts() // importare produse
    {
        Path filePath = Paths.get("products.csv"); // fisier
        assert filePath!=null;
        try (Stream<String> lines = Files.lines( filePath )) // luarea pe stream
        {
            List<List<String>> values = lines
                    .skip(1) // nu iau prima linie
                    .map(line -> Arrays.asList(line.split(","))) // despartitor
                    .filter(list -> list.get(6) != null)
                    .collect(Collectors.toList());

            for(List<String> line : values){ // verificare de duplicate si creare baseProduct
                    BaseProduct baseProduct = new BaseProduct(line.get(0),Double.parseDouble(line.get(1)),Integer.parseInt(line.get(2)),Integer.parseInt(line.get(3)),Integer.parseInt(line.get(4)),Integer.parseInt(line.get(5)),Integer.parseInt(line.get(6)));
                    if(noDuplicates(line.get(0))) items.add(baseProduct);
            }

            Serializator.serializator(this); // memorare
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    } // metoda de generare de rapoarte folosind streamuri cu urmatoarele conditii

    /**
     * @param startTime Timp start
     * @param endTime Timp terminare
     * @param productsOrderMoreThan Produse comandate mai mult de
     * @param clientsOrderedMoreThan Clienti comandate mai mult de
     * @param valueOrder valoare comanda
     * @param productsSpecifiedDay data de unde se face comanda
     * @throws ParseException
     */
    public void generateReports(int startTime,int endTime,int productsOrderMoreThan,int clientsOrderedMoreThan,int valueOrder,int productsSpecifiedDay) throws ParseException {
        assert (startTime>-1 && endTime<13 && productsOrderMoreThan >-1 && clientsOrderedMoreThan>-1 && valueOrder>0 && productsSpecifiedDay>-1);
        task1 = orders.keySet().stream().filter(t -> t.getDate().get(Calendar.HOUR) >=startTime &&  t.getDate().get(Calendar.HOUR) <=endTime ).collect(Collectors.toSet()); // conditie
        //2
        ArrayList<MenuItem> menuItems = new ArrayList<>(); // lista de menuItem
        orders.values().forEach(menuItems::addAll); // iterator
        task2 = menuItems.stream().filter(i -> Collections.frequency(menuItems, i) >productsOrderMoreThan).collect(Collectors.toSet());
        //3
        ArrayList<Client> clients = new ArrayList<>(); // lista pt clienti
        Set<Order> orderPrice = orders.keySet().stream().filter(i -> i.getPrice()>valueOrder).collect(Collectors.toSet());
        for(Order order : orderPrice) clients.add(order.getClient()); // adaugare comenzi
        task3 = clients.stream().filter(t -> Collections.frequency(clients,t) > clientsOrderedMoreThan ).collect(Collectors.toSet());
        //4
        Set<Order> days = orders.keySet().stream().filter(t -> t.getDate().get(Calendar.DAY_OF_MONTH) ==productsSpecifiedDay).collect(Collectors.toSet());
        HashMap<Order, ArrayList<MenuItem>> ord = new HashMap<>(); // tot cu stream
        for(Order order : days) ord.put(order,orders.get(order)); // toate comenzile
        ArrayList<MenuItem> arrayList =   new ArrayList<>();
        ord.values().forEach(arrayList::addAll);
        this.task4 = new HashSet<>(arrayList); // calculez frecventa
        this.task4.stream().forEach(t ->{task4L.add(Collections.frequency(arrayList,t)) ; System.out.println(t.getTitlee()+" -> "+Collections.frequency(arrayList,t));});
    }

    /**
     * @param menuItems meniurile alese
     * @param order comanda
     */
    public void makeOrder(ArrayList<MenuItem> menuItems,Order order){
        assert true; // efectuare comanda
        HashMap<Order, ArrayList<MenuItem>> ordersOld = orders; // comenzi
        orders.put(order,menuItems);
        lastOrder = order;
        pcs.firePropertyChange("Order", "ok", "okkk"); //lansare trigger employee
        Serializator.serializator(this);
    }

    /**
     * @param title titlu
     * @param rating rating
     * @param calories calories
     * @param protein protein
     * @param fat fat
     * @param sodium sodium
     * @param price price
     * @return
     */
    public ArrayList<MenuItem> searchForProducts(String title,double rating, int calories,int protein,int fat,int sodium,int price){
        assert (title!=null || rating!=-1 || calories!=-1 || protein!=-1 || fat!=-1 || sodium!=-1 || price!=-1);
        if (title!=null) { // cautare dupa fiecare camp
            Set<MenuItem> products = items.stream().filter(t -> t.getTitlee().contains(title)).collect(Collectors.toSet());
            return new ArrayList<>(products);
        }
        if (rating!=-1) {
            Set<MenuItem> products = items.stream().filter(t -> t.getRating()==rating).collect(Collectors.toSet());
            return new ArrayList<>(products);
        }
        if (calories!=-1) {
            Set<MenuItem> products = items.stream().filter(t -> t.getCalories()==calories).collect(Collectors.toSet());
            return new ArrayList<>(products);
        }
        if (protein!=-1) {
            Set<MenuItem> products = items.stream().filter(t -> t.getProtein()==protein).collect(Collectors.toSet());
            return new ArrayList<>(products);
        }
        if (fat!=-1) {
            Set<MenuItem> products = items.stream().filter(t -> t.getFat()==fat).collect(Collectors.toSet());
            return new ArrayList<>(products);
        }
        if (sodium!=-1) {
            Set<MenuItem> products = items.stream().filter(t -> t.getSodium()==sodium).collect(Collectors.toSet());
            return new ArrayList<>(products);
        }
        if (price!=-1) {
            Set<MenuItem> products = items.stream().filter(t -> t.getPrice()==price).collect(Collectors.toSet());
            return new ArrayList<>(products);
        }
        return null;
    }

    /**
     * @return
     */
    public HashSet<MenuItem> getItems() {
        return items;
    }
    /**
     *
     */
    private boolean noDuplicates(String title)
    {
        for(MenuItem menuItem : items)
        {
            if(title.equals(menuItem.getTitlee()))
                return false;
        }
        return true;
    }
    /**
     * @param name
     * @return
     */
    public MenuItem searchName(String name)
    {
        for(MenuItem menuItem : items)
        {
            if(name.equals(menuItem.getTitlee()))
                return menuItem;
        }
        return null;
    }
    /**
     * @param client
     */
    public void addClient(Client client)
    {
        registeredClients.add(client);
        Serializator.serializator(this);
    }
    /**
     *
     */
    public void allClient() {
        for(Client client : registeredClients)
        {
            System.out.println(client.getUsername());
        }
    }
    /**
     * @param username
     * @param password
     * @return
     */
    public Client usernameMatch(String username,String password) {
        for(Client client : registeredClients)
        {
            if(client.getUsername().equals(username) && client.getPassword().equals(password))
                return client;
        }
        return null;
    }
    public Set<Order> getTask1() {
        return task1;
    }

    public Set<MenuItem> getTask2() {
        return task2;
    }

    public Set<Client> getTask3() {
        return task3;
    }

    public HashSet<MenuItem> getTask4() {
        return task4;
    }

    public ArrayList<Integer> getTask4L() {
        return task4L;
    }
    public Order getLastOrder()
    {
        return lastOrder;
    }

    public Client getAdministrator() {
        return administrator;
    }

    public Client getEmployee() {
        return employee;
    }
}
