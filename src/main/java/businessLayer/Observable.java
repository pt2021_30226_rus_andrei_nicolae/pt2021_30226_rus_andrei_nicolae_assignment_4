package businessLayer;

import java.beans.*;

/**
 *
 */
public abstract class Observable {
    protected PropertyChangeSupport pcs = new  PropertyChangeSupport(this);

    /**
     *
     * @param l
     */
    public void addObserver(PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }

}


