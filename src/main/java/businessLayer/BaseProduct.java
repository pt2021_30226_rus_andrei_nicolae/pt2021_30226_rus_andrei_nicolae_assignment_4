package businessLayer;

import java.io.Serializable;

/**
 * Clasa de baza produs
 */
public class BaseProduct extends MenuItem implements Serializable {
    private String title;
    private double rating;
    private int calories,protein,fat,sodium,price;

    /**
     *
     * @param title
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     */
    public BaseProduct(String title,double rating,int calories,int protein,int fat,int sodium,int price)
    {
        super(title,rating,calories,protein,fat,sodium,price);
        this.title=title;
        this.rating=rating;
        this.rating=rating;
        this.calories=calories;
        this.protein=protein;
        this.fat=fat;
        this.sodium=sodium;
        this.price=price;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString()
    {
        return "Name: "+title+" Rating: "+rating+" Calories: "+calories+" Protein: "+protein+" Sodium: "+sodium+" Fat: "+fat+" Price: "+price;
    }

    /**
     *
     * @return
     */
    public String getTitle()
    {
        return title;
    }
}
