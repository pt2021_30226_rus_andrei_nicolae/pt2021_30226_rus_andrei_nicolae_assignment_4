package businessLayer;

import java.io.Serializable;

/**
 *
 */
public class MenuItem implements Serializable { // clasa de meniu

    private String title; // campuri
    private double rating;
    private int calories,protein,fat,sodium,price;

    /**
     *
     * @param title
     * @param rating
     * @param calories
     * @param protein
     * @param fat
     * @param sodium
     * @param price
     */
    public MenuItem(String title,double rating,int calories,int protein,int fat,int sodium,int price) // initializare
    {   // constructor
        this.title=title;
        this.rating=rating;
        this.calories=calories;
        this.protein=protein;
        this.sodium=sodium;
        this.fat=fat;
        this.price=price;
    }

    /**
     *
     */
    public MenuItem()
    {

    }

    /**
     *
     * @return
     */
    public String toStringg() // convertire la sir
    {
        return "Name: "+title+" Rating: "+rating+" Calories: "+calories+" Protein: "+protein+" Sodium: "+sodium+" Fat: "+fat+" Price: "+price;
    }

    /**
     *
     * @return
     */
    public String getTitlee()
    {
        return title;
    }

    /**
     *
     * @return
     */
    public int getPrice() {
        return price;
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @return
     */
    public double getRating() {
        return rating;
    }

    /**
     *
     * @return
     */
    public int getCalories() {
        return calories;
    }

    /**
     *
     * @return
     */
    public int getProtein() {
        return protein;
    }

    /**
     *
     * @return
     */
    public int getFat() {
        return fat;
    }

    /**
     *
     * @return
     */
    public int getSodium() {
        return sodium;
    }

    /**
     *
     * @param menuItem
     */
    public void setFields(MenuItem menuItem) // setare campuri
    {
        this.title=menuItem.getTitle();
        this.rating=menuItem.getRating();
        this.calories=menuItem.getCalories();
        this.protein=menuItem.getProtein();
        this.fat=menuItem.getFat();
        this.sodium=menuItem.getSodium();
        this.fat=menuItem.getFat();
    }
}
