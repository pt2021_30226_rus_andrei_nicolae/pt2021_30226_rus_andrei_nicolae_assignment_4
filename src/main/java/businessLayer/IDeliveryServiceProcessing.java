package businessLayer;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Interfata DeliveryService
 */
public interface IDeliveryServiceProcessing {
    /**
     * Importa lista de produce
     * @pre filePath!= null
     * @post @forall line:[values]
     *       baseProduct create
     */
    public void importProducts();

    /**
     * Sterge din lista un element
     * @param name Name to  be deleted
     * @pre name!= null
     * @post menuItem==name
     */
    public void deleteProduct(String name);

    /**
     * Cauta si modifica produsul
     * @param name Nume de cautat
     * @param menuItem Nume de editat
     * @pre name!= null
     * @post @forall k : [1 .. items ()  ]
     * @post name=menuItem
     *       [add(menuitem)]
     */
    public void editProduct(String name,MenuItem menuItem);

    /**
     * Adauga elementul in lsta
     * @param baseProduct produsul de baza de adaugat
     * @pre true
     * @post @nochange
     */
    public void addProduct(BaseProduct baseProduct);

    /**
     * Genereaza rapoarte
     * @param startTime Timp start
     * @param endTime Timp terminare
     * @param productsOrderMoreThan Produse comandate mai mult de
     * @param clientsOrderedMoreThan Clienti comandate mai mult de
     * @param valueOrder valoare comanda
     * @param productsSpecifiedDay data de unde se face comanda
     * @throws ParseException ex
     * @pre startTime,endTime,productsOrderMoreThan,clientsOrderedMoreThan,valueOrder,productsSpecifiedDay,ParseException != null
     * @post @forall menuItem,client,order : [items,clients,orders ] @getElementBasedOnCriteria
     */
    public void generateReports(int startTime,int endTime,int productsOrderMoreThan,int clientsOrderedMoreThan,int valueOrder,int productsSpecifiedDay) throws ParseException;

    /**
     * Metoda ce creaza o comanda
     * @param menuItems meniurile alese
     * @param order comanda
     * @pre true
     * @post fireProperty
     */
    public void makeOrder(ArrayList<MenuItem> menuItems, Order order);

    /**
     *
     * @param title titlu
     * @param rating rating
     * @param calories calories
     * @param protein protein
     * @param fat fat
     * @param sodium sodium
     * @param price price
     * @pre title,rating,calories,protein,fat,sodium,price !=-1 / null
     * @post @forall menuItem : items (filter based on field)
     * @return products-result
     */
    public ArrayList<MenuItem> searchForProducts(String title,double rating, int calories,int protein,int fat,int sodium,int price);
}
