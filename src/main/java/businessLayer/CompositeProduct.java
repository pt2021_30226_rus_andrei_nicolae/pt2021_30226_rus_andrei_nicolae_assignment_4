package businessLayer;

import java.util.ArrayList;

/**
 *
 */
public  class CompositeProduct extends MenuItem{

    ArrayList<MenuItem> menuItems; // lista de produse din fiecare produs

    /**
     *
     */
    public CompositeProduct()
    {
        menuItems = new ArrayList<>();
    } // initializarea produsului compus

    /**
     *
     * @param menuItem
     */
    public  void addComposite(MenuItem menuItem)
    {
        menuItems.add(menuItem);
    } // adaugare produs

    /**
     *
     * @return
     */
    public int comutePrice() // calcularea sumei
    {
        int suma=0;
        for(MenuItem menuItem : menuItems)
        {
            suma += menuItem.getPrice();
        }
        return suma;
    }

    /**
     *
     * @return
     */
    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    } // returnarea ei
}
