package presentationLayer;

import businessLayer.DeliveryService;

import javax.swing.*;
import java.beans.*;
import java.util.Calendar;

/**
 *
 */
public class ViewEmployee extends JFrame implements PropertyChangeListener {
    private JLabel orderList;
    private DeliveryService deliveryService;
    private JPanel panel;

    /**
     *
     * @param deliveryService
     */
    public ViewEmployee (DeliveryService deliveryService)
    {
        this.deliveryService = deliveryService;
        deliveryService.addObserver(this);
        orderList = new JLabel("Orders trigger: ");
        panel = new JPanel();
        panel.add(orderList);
        this.add(panel);
        this.setSize(215,400);
        this.setVisible(true);
    }

    /**
     *
     * @param deliveryService
     */
    public void refreshDelivery(DeliveryService deliveryService)
    {
        this.deliveryService = deliveryService;
    }
    @Override
    /**
     *
     */
    public void propertyChange(PropertyChangeEvent evt) {
        Calendar calendar = Calendar.getInstance();
        panel.add(new JLabel("A new order has been made at: "+ calendar.getTime()));
        this.add(panel);
    }
}
