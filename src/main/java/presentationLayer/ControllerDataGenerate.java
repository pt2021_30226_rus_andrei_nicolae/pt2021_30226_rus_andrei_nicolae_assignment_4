package presentationLayer;

import businessLayer.Client;
import businessLayer.DeliveryService;
import businessLayer.MenuItem;
import businessLayer.Order;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class ControllerDataGenerate {
    private ViewDataGenerate viewDataGenerate;
    private DeliveryService deliveryService;

    /**
     *
     * @param deliveryService
     */
    public ControllerDataGenerate(DeliveryService deliveryService)
    {
        viewDataGenerate = new ViewDataGenerate();
        this.deliveryService = deliveryService;
        viewDataGenerate.addGenerateListener(new Generate());
    }
    class Generate implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent)
        {
            int i=0;
            try {
                deliveryService.generateReports(Integer.parseInt(viewDataGenerate.getStartTime().getText()),Integer.parseInt(viewDataGenerate.getEndTime().getText()),Integer.parseInt(viewDataGenerate.getProductsOrderMoreThan().getText()),Integer.parseInt(viewDataGenerate.getClientsOrderedMoreThan().getText()),Integer.parseInt(viewDataGenerate.getValueOrder().getText()),Integer.parseInt(viewDataGenerate.getProductsSpecifiedDay().getText()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Set<Order> task1=deliveryService.getTask1();
            Set<MenuItem> task2=deliveryService.getTask2();
            Set<Client> task3=deliveryService.getTask3();
            HashSet<MenuItem> task4=deliveryService.getTask4();
            ArrayList<Integer> task4L=deliveryService.getTask4L();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<html><br>****TASK 1 --- Time interval of the orders – a report should be generated with the orders performed between a given start hour and a given end hour regardless the date.<br>");
            for(Order order : task1) stringBuilder.append( "</br>"+order.toString() +"<br>");
            stringBuilder.append("<html><br>****TASK 2 --- the products ordered more than a specified number of times so far <br>");
            for(MenuItem menuItem : task2) stringBuilder.append( "</br>"+menuItem.toStringg() +"<br>");
            stringBuilder.append("<html><br>****TASK 3 --- the clients that have ordered more than a specified number of times and the value of the order was higher than a specified amount.<br>");
            for(Client client : task3) stringBuilder.append( "</br>"+client.toString() +"<br>");
            stringBuilder.append("<html><br>****TASK 4 --- the products ordered within a specified day with the number of times they have been ordered.<br>");

            for(MenuItem menuItem : task4)
            {
                stringBuilder.append( "</br>"+menuItem.toStringg() + " ->  and was ordered: "+task4L.get(i) +"<br>");
                i++;
                System.out.println("OJ");
            }
            viewDataGenerate.addResult(stringBuilder.toString());
        }

    }
}
