package presentationLayer;

import businessLayer.*;
import dataLayer.FileWriterr;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import static java.lang.Math.abs;

/**
 *
 */
public class ControllerClient {
    private ViewClient viewClient;
    private DeliveryService deliveryService;
    private CompositeProduct compositeProduct;
    private Client client;

    /**
     *
     * @param deliveryService
     * @param client
     */
    public ControllerClient(DeliveryService deliveryService,Client client)
    {
        compositeProduct = new CompositeProduct();
        this.deliveryService=deliveryService;
        viewClient= new ViewClient(deliveryService);
        viewClient.addItemListener(new AddItem());
        viewClient.addOrderListener(new Order());
        viewClient.addSearchBListener(new Search());
        this.client=client;
    }
    class AddItem implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
            viewClient.editList(viewClient.getItem());
            MenuItem menuItem  = deliveryService.searchName(viewClient.getItem());
            System.out.println(menuItem.getTitlee());
            compositeProduct.addComposite(menuItem);
        }
    }
    class Order implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
            Calendar date = Calendar.getInstance();
            businessLayer.Order order = new businessLayer.Order(abs(new Random().nextInt()%10000+500),date,compositeProduct.comutePrice(),client);
            deliveryService.makeOrder(compositeProduct.getMenuItems(),order);
            try {
                FileWriterr.fileWrite(order,compositeProduct.getMenuItems());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    class Search implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
            String title=null;
            double rating=-1;
            int calories=-1, protein=-1, fat=-1, sodium=-1, price=-1;
            switch (viewClient.getSearchPosibilities().getSelectedItem().toString()) {
                case "title" -> title = viewClient.getSearch().getText();
                case "rating" -> rating = Double.parseDouble(viewClient.getSearch().getText());
                case "calories" -> calories = Integer.parseInt(viewClient.getSearch().getText());
                case "protein" -> protein = Integer.parseInt(viewClient.getSearch().getText());
                case "fat" -> fat = Integer.parseInt(viewClient.getSearch().getText());
                case "sodium" -> sodium = Integer.parseInt(viewClient.getSearch().getText());
                case "price" -> price = Integer.parseInt(viewClient.getSearch().getText());
            }
            ArrayList<MenuItem> newList = deliveryService.searchForProducts(title,rating,calories,protein,fat,sodium,price);
            String [] list = new String[newList.size()];
            int i=0;
            for(MenuItem menuItem : newList){
                list[i]=menuItem.getTitlee();
                i++;
            }
            viewClient.editListItems(list);
        }
    }
}
