package presentationLayer;

import businessLayer.Client;
import businessLayer.DeliveryService;
import presentationLayer.ControllerAdmin;
import presentationLayer.ControllerClient;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class ControllerLogIn {
    private ViwLogIn viwLogIn;
    private DeliveryService deliveryService;

    /**
     *
     * @param deliveryService
     */
    public ControllerLogIn(DeliveryService deliveryService)
    {
        viwLogIn = new ViwLogIn();
        this.deliveryService = deliveryService;
        viwLogIn.addConfirmListener(new Confirm());
        viwLogIn.addRegisterListener(new Register());
    }

    class Confirm implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
            if(viwLogIn.getClientOption())
            {
                Client client = deliveryService.usernameMatch(viwLogIn.getUsername(), viwLogIn.getPassword());
                if(client == null) JOptionPane.showMessageDialog(null, "This client does not exist, please register or retype the data input again!", "InfoBox: " + "Unexisting client", JOptionPane.ERROR_MESSAGE);
                else new ControllerClient(deliveryService,client);
            }
            if(viwLogIn.getEmployeeOption())
            {
                if(deliveryService.getEmployee().getUsername().equals(viwLogIn.getUsername()) && deliveryService.getEmployee().getPassword().equals(viwLogIn.getPassword()))
                new ViewEmployee(deliveryService);
                else JOptionPane.showMessageDialog(null, "Wrong employee input!", "InfoBox: " + "Wrong employee", JOptionPane.ERROR_MESSAGE);
            }
            if(viwLogIn.getAdminOption())
            {
                if(deliveryService.getAdministrator().getUsername().equals(viwLogIn.getUsername()) && deliveryService.getAdministrator().getPassword().equals(viwLogIn.getPassword()))
                new ControllerAdmin(deliveryService);
                else JOptionPane.showMessageDialog(null, "Wrong admin input!", "InfoBox: " + "Wrong admin", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    class Register implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
            Client client = new Client(viwLogIn.getUsername(),viwLogIn.getPassword());
            deliveryService.addClient(client);
            deliveryService.allClient();
        }
    }
}
