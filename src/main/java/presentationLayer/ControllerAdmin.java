package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.DeliveryService;
import businessLayer.MenuItem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class ControllerAdmin {
    private ViewAdmin viewAdmin;
    private DeliveryService deliveryService;

    /**
     *
     * @param deliveryService
     */
    public ControllerAdmin(DeliveryService deliveryService)
    {
        viewAdmin = new ViewAdmin(deliveryService);
        this.deliveryService = deliveryService;
        viewAdmin.addGenerateListener(new Generate());
        viewAdmin.addPopulateListener(new Populate());
        viewAdmin.addProductManListener(new ProductManager());
    }

    class Populate implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
           deliveryService.importProducts();
        }
    }
    class ProductManager implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
            if(viewAdmin.getOption().equals("Add"))
            {
                BaseProduct baseProduct = new BaseProduct(viewAdmin.getTitle(), viewAdmin.getRating(),viewAdmin.getCalories(),viewAdmin.getProteins(),viewAdmin.getFats(),viewAdmin.getSodium(),viewAdmin.getPrice());
                deliveryService.addProduct(baseProduct);
                System.out.println(viewAdmin.getTitle()+ " " +baseProduct);
            }
            if(viewAdmin.getOption().equals("Modify"))
            {
                MenuItem menuItem = new MenuItem(viewAdmin.getTitle(), viewAdmin.getRating(),viewAdmin.getCalories(),viewAdmin.getProteins(),viewAdmin.getFats(),viewAdmin.getSodium(),viewAdmin.getPrice());
                deliveryService.editProduct(viewAdmin.getProducts(),menuItem);
            }
            if(viewAdmin.getOption().equals("Delete"))
            {
                deliveryService.deleteProduct(viewAdmin.getProducts());
            }
        }
    }
    class Generate implements ActionListener
    {
        public void actionPerformed(ActionEvent actionEvent)
        {
            new ControllerDataGenerate(deliveryService);
        }
    }
}
