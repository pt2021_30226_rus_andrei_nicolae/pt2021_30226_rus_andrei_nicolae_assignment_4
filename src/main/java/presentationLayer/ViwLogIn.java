package presentationLayer;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 *
 */
public class ViwLogIn extends JFrame {
    private JTextField username,password;
    private JLabel usernameL,passwordL;
    private JButton confirm,registerClient;
    private JRadioButton client,admin,employee;

    /**
     *
     */
    public ViwLogIn()
    {
        username = new JTextField(10);
        password = new JTextField(10);
        usernameL = new JLabel("Unsername: ");
        passwordL = new JLabel("Password: ");
        confirm = new JButton("Confirm");
        registerClient = new JButton("Add new client");
        client = new JRadioButton("Client ");
        admin = new JRadioButton("Admin ");
        employee = new JRadioButton("Employee ");
        JPanel p1 = new JPanel();
        ButtonGroup bg1 = new ButtonGroup( );
        bg1.add(client);
        bg1.add(admin);
        bg1.add(employee);
        p1.add(usernameL);
        p1.add(username);
        p1.add(passwordL);
        p1.add(password);
        p1.add(confirm);
        p1.add(registerClient);
        p1.add(client);
        p1.add(admin);
        p1.add(employee);
        this.add(p1);
        this.setSize(250,200);
        this.setVisible(true);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     *
     * @param actionListener
     */
    public void addConfirmListener(ActionListener actionListener)
    {
        confirm.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addRegisterListener(ActionListener actionListener)
    {
        registerClient.addActionListener(actionListener);
    }

    /**
     *
     * @return
     */
    public boolean getClientOption()
    {
        return client.isSelected();
    }

    /**
     *
     * @return
     */
    public boolean getAdminOption()
    {
        return admin.isSelected();
    }

    /**
     *
     * @return
     */
    public boolean getEmployeeOption()
    {
        return employee.isSelected();
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username.getText();
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password.getText();
    }
}
