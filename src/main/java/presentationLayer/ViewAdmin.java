package presentationLayer;

import businessLayer.BaseProduct;
import businessLayer.DeliveryService;
import businessLayer.MenuItem;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.List;

/**
 *
 */
public class ViewAdmin extends JFrame {
    private JButton populate,productMan,generate;
    private JComboBox products,option;
    private JLabel operation, titleL, ratingL, caloriesL, proteinsL, fatsL, sodiumL, priceL;
    private JTextField title, rating, calories, proteins, fats, sodium, price;
    private  DeliveryService deliveryService;

    /**
     *
     * @param deliveryService
     */
    public ViewAdmin(DeliveryService deliveryService)
    {
        this.deliveryService = deliveryService;
        populate = new JButton("Populate with data");
        productMan = new JButton("Manage Products");
        generate = new JButton("Generate data");
        products = new JComboBox(options());
        String[] op = {"Add","Delete","Modify","Create"};
        option = new JComboBox(op);
        operation = new JLabel("Operation on product: ");
        titleL=new JLabel("Title:");
        ratingL= new JLabel("Rating:");
        caloriesL= new JLabel("Calories:");
        proteinsL= new JLabel("Proteins:");
        fatsL = new JLabel("Fats:");
        sodiumL = new JLabel("Sodium:");
        priceL = new JLabel("Price:");

        title = new JTextField(10);
        rating = new JTextField(10);
        calories = new JTextField(10);
        proteins = new JTextField(10);
        fats = new JTextField(10);
        sodium = new JTextField(10);
        price = new JTextField(10);

        JPanel p1 = new JPanel();
        p1.add(products);
        p1.add(titleL);
        p1.add(title);
        p1.add(ratingL);
        p1.add(rating);
        p1.add(caloriesL);
        p1.add(calories);
        p1.add(proteinsL);
        p1.add(proteins);
        p1.add(fatsL);
        p1.add(fats);
        p1.add(sodiumL);
        p1.add(sodium);
        p1.add(priceL);
        p1.add(price);
        p1.add(operation);
        p1.add(option);
        p1.add(populate);
        p1.add(productMan);
        p1.add(generate);
        this.add(p1);
        this.setSize(215,400);
        this.setVisible(true);
    }

    /**
     *
     * @return
     */
    public String getOption()
    {
        return option.getSelectedItem().toString();
    }

    /**
     *
     * @return
     */
    public int getPrice() {
        System.out.println(Integer.parseInt(price.getText()));
        return Integer.parseInt(price.getText());
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return title.getText();
    }

    /**
     *
     * @return
     */
    public Double getRating() {
        return Double.parseDouble(rating.getText());
    }

    /**
     *
     * @return
     */
    public int getCalories() {
        return Integer.parseInt(calories.getText());
    }

    /**
     *
     * @return
     */
    public int getProteins() {
        return Integer.parseInt(proteins.getText());
    }

    /**
     *
     * @return
     */
    public int getFats() {
        return Integer.parseInt(fats.getText());
    }

    /**
     *
     * @return
     */
    public int getSodium() {
        return Integer.parseInt(sodium.getText());
    }

    /**
     *
     * @param actionListener
     */
    public void addPopulateListener(ActionListener actionListener)
    {
        populate.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addProductManListener(ActionListener actionListener)
    {
        productMan.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addGenerateListener(ActionListener actionListener)
    {
        generate.addActionListener(actionListener);
    }

    /**
     *
     * @return
     */
    private  String [] options(){
        String [] options = new String[100000];
        int i=0;
        for(MenuItem menuItem : deliveryService.getItems())
        {
            options[i] =menuItem.getTitlee();
            i++;
        }
        return options;
    }

    /**
     *
     * @return
     */
    public String getProducts() {
        return products.getSelectedItem().toString();
    }
}
