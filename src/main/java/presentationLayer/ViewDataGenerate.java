package presentationLayer;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 *
 */
public class ViewDataGenerate extends JFrame {
    private JTextField startTime,endTime,productsOrderMoreThan,clientsOrderedMoreThan,valueOrder,productsSpecifiedDay;
    private JLabel startTimeL,endTimeL,productsOrderMoreThanL,clientsOrderedMoreThanL,valueOrderL,productsSpecifiedDayL;
    private JButton generate;

    /**
     *
     */
    public ViewDataGenerate()
    {
        startTime = new JTextField(10);
        endTime = new JTextField(10);
        productsOrderMoreThan = new JTextField(10);
        clientsOrderedMoreThan = new JTextField(10);
        valueOrder = new JTextField(10);
        productsSpecifiedDay = new JTextField(10);

        startTimeL = new JLabel("Time start: ");
        endTimeL = new JLabel("Time end: ");
        productsOrderMoreThanL = new JLabel("Products ordered more than a specified number of time: ");
        clientsOrderedMoreThanL = new JLabel("clients that have ordered more than a specified number of times: ");
        valueOrderL = new JLabel("Order value greater than: ");
        productsSpecifiedDayL = new JLabel("Products ordered within a specified day");
        generate = new JButton("Generate");

        JPanel panel = new JPanel();
        panel.add(startTimeL);
        panel.add(startTime);
        panel.add(endTimeL);
        panel.add(endTime);
        panel.add(productsOrderMoreThanL);
        panel.add(productsOrderMoreThan);
        panel.add(clientsOrderedMoreThanL);
        panel.add(clientsOrderedMoreThan);
        panel.add(valueOrderL);
        panel.add(valueOrder);
        panel.add(productsSpecifiedDayL);
        panel.add(productsSpecifiedDay);
        panel.add(generate);
        this.add(panel);
        this.setSize(215,400);
        this.setVisible(true);
    }

    /**
     *
     * @param e
     */
    public void addGenerateListener(ActionListener e)
    {
        generate.addActionListener(e);
    }

    /**
     *
     * @param s
     */
    public void addResult(String s)
    {
        this.add(new JLabel(s), SwingConstants.CENTER);
    }

    /**
     *
     * @return
     */
    public JTextField getStartTime() {
        return startTime;
    }

    /**
     *
     * @return
     */
    public JTextField getEndTime() {
        return endTime;
    }

    /**
     *
     * @return
     */
    public JTextField getProductsOrderMoreThan() {
        return productsOrderMoreThan;
    }

    /**
     *
     * @return
     */
    public JTextField getClientsOrderedMoreThan() {
        return clientsOrderedMoreThan;
    }

    /**
     *
     * @return
     */
    public JTextField getValueOrder() {
        return valueOrder;
    }

    /**
     *
     * @return
     */
    public JTextField getProductsSpecifiedDay() {
        return productsSpecifiedDay;
    }

    /**
     *
     * @return
     */
    public JLabel getStartTimeL() {
        return startTimeL;
    }

    /**
     *
     * @return
     */
    public JLabel getEndTimeL() {
        return endTimeL;
    }

    /**
     *
     * @return
     */
    public JLabel getProductsOrderMoreThanL() {
        return productsOrderMoreThanL;
    }

    /**
     *
     * @return
     */
    public JLabel getClientsOrderedMoreThanL() {
        return clientsOrderedMoreThanL;
    }

    /**
     *
     * @return
     */
    public JLabel getValueOrderL() {
        return valueOrderL;
    }

    /**
     *
     * @return
     */
    public JLabel getProductsSpecifiedDayL() {
        return productsSpecifiedDayL;
    }

    /**
     *
     * @return
     */

    public JButton getGenerate() {
        return generate;
    }
}
