package presentationLayer;

import businessLayer.DeliveryService;
import businessLayer.MenuItem;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 *
 */
public class ViewClient extends JFrame {
    private JButton addItem,order,searchB;
    private JComboBox products,searchPosibilities;
    private JLabel productsL,searchL;
    private JTextField search;
    private DeliveryService deliveryService;
    private JLabel listCart;
    private JPanel p1 =new JPanel();

    /**
     *
     * @param deliveryService
     */
    public ViewClient(DeliveryService deliveryService)
    {
        this.deliveryService=deliveryService;
        addItem= new JButton("Adauga produs");
        order = new JButton("Confirma comanda");
        products = new JComboBox(options());
        productsL = new JLabel("Lista produse: ");
        searchL = new JLabel("Motot cautare");
        search= new JTextField(10);
        listCart = new JLabel("List cart: ");
        searchB = new JButton("Search");
        String [] searchOp = {"title","rating","calories","protein","fat","sodium","price"};
        searchPosibilities = new JComboBox(searchOp);
        p1.add(productsL);
        p1.add(products);
        p1.add(searchPosibilities);
        p1.add(searchL);
        p1.add(search);
        p1.add(searchB);
        p1.add(addItem);
        p1.add(order);
        p1.add(listCart);
        this.add(p1);
        this.setSize(215,400);
        this.setVisible(true);
    }

    /**
     *
     * @param actionListener
     */
    public void addItemListener(ActionListener actionListener)
    {
        addItem.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addSearchBListener(ActionListener actionListener)
    {
        searchB.addActionListener(actionListener);
    }

    /**
     *
     * @param actionListener
     */
    public void addOrderListener(ActionListener actionListener)
    {
        order.addActionListener(actionListener);
    }

    /**
     *
     * @return
     */
    private  String [] options(){
        String [] options = new String[100000];
        int i=0;
        for(MenuItem menuItem : deliveryService.getItems())
        {
            options[i] =menuItem.getTitlee();
            i++;
        }
        return options;
    }

    /**
     *
     * @param element
     */
    public void editList(String element)
    {
        String before = listCart.getText();
        p1.remove(listCart);
        this.remove(p1);
        listCart = new JLabel(before+", "+element);
        p1.add(listCart);
        this.add(p1);
    }

    /**
     *
     * @return
     */
    public String getItem()
    {
        return products.getSelectedItem().toString();
    }

    /**
     *
     * @param list
     */
    public void editListItems(String [] list)
    {
        p1.remove(products);
        this.remove(p1);
        products = new JComboBox<>(list);
        p1.add(products);
        this.add(p1);
    }

    /**
     *
     * @return
     */
    public JComboBox getSearchPosibilities() {
        return searchPosibilities;
    }

    /**
     *
     * @return
     */
    public JTextField getSearch() {
        return search;
    }
}
