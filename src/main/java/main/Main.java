package main;

import businessLayer.DeliveryService;
import dataLayer.Serializator;
import presentationLayer.ControllerLogIn;

import java.io.File;

/**
 *
 */
public class Main {
    public static void main(String args[]) {
        DeliveryService deliveryService1 = Serializator.deserializator(); // deserializator
        DeliveryService deliveryService = new DeliveryService();
        File file = new File("serializator.ser"); // fisier
        if (file.isFile()) new ControllerLogIn(deliveryService1);
        else new ControllerLogIn(deliveryService);

    }
}
